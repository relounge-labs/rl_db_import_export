<?php

$EM_CONF['rl_db_import_export'] = [
	'title' => 'Database to JSON Import/Export',
	'description' => '',
	'category' => 'services',
	'author' => '',
	'author_email' => '',
	'state' => 'stable',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '1.0.0',
	'constraints' => [
		'depends' => [
			'typo3' => '12.0.0-12.9.99',
		],
		'conflicts' => [],
		'suggests' => [],
	],
];
