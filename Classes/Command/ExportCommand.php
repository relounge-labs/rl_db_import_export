<?php

declare(strict_types=1);

namespace Relounge\RlDbImportExport\Command;

use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\Exception\TableNotFoundException;
use Relounge\RlDbImportExport\Service\DatabaseService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Core\Environment;

class ExportCommand extends Command {

	/**
	 * @var \Relounge\RlDbImportExport\Service\DatabaseService
	 */
	private DatabaseService $databaseService;

	/**
	 * @noinspection PhpUnused
	 * @param \Relounge\RlDbImportExport\Service\DatabaseService $databaseService
	 * @return void
	 */
	public function injectDatabaseService(DatabaseService $databaseService): void {
		$this->databaseService = $databaseService;
	}

	/**
	 * @return void
	 */
	protected function configure(): void {
		$this->setDescription('');
		$this->addArgument('table', InputArgument::REQUIRED, 'The name of the table to be exported.');
		$this->addArgument('target', InputArgument::REQUIRED, 'Path to which the JSON file should be written.');
		$this->addArgument('chunkSize', InputArgument::OPTIONAL, 'Number of entries per file. Reduce this value if the memory is insufficient.', 100);
	}

	/**
	 * @param \Symfony\Component\Console\Input\InputInterface $input
	 * @param \Symfony\Component\Console\Output\OutputInterface $output
	 * @return int
	 */
	protected function execute(InputInterface $input, OutputInterface $output): int {
		$io = new SymfonyStyle($input, $output);
		$table = $input->getArgument('table');
		$target = realpath(Environment::getProjectPath() . DIRECTORY_SEPARATOR . $input->getArgument('target'));
		$chunkSize = (int)$input->getArgument('chunkSize');
		$io->writeln(sprintf('Export table "%s" to "%s" ...', $table, $target));
		try {
			$io->success(sprintf('%s rows exported.', $this->databaseService->dumpTableToJsonFile($table, $target, $chunkSize)));
			return Command::SUCCESS;
		} catch (TableNotFoundException $e) {
			// @extensionScannerIgnoreLine
			$io->error(sprintf('Table %s does not exist!', $table));
		} catch (Exception $e) {
			// @extensionScannerIgnoreLine
			$io->error($e->getMessage());
		}
		return Command::FAILURE;
	}
}
