<?php

declare(strict_types=1);

namespace Relounge\RlDbImportExport\Service;

use DirectoryIterator;
use Generator;
use TYPO3\CMS\Core\Database\ConnectionPool;

class DatabaseService {

	/**
	 * @var \TYPO3\CMS\Core\Database\ConnectionPool
	 */
	private ConnectionPool $connectionPool;

	/**
	 * @param \TYPO3\CMS\Core\Database\ConnectionPool $connectionPool
	 */
	public function __construct(ConnectionPool $connectionPool) {
		$this->connectionPool = $connectionPool;
	}

	/**
	 * @param string $table
	 * @param string $dir
	 * @param int $chunkSize
	 * @return int
	 * @throws \Doctrine\DBAL\Exception
	 */
	public function dumpTableToJsonFile(string $table, string $dir, int $chunkSize): int {
		if (!is_dir($dir)) {
			mkdir($dir, 0777, true);
		}
		foreach ($this->getFiles($dir, $table) as $file) {
			unlink($file->getRealPath());
		}
		$rows = $this->connectionPool->getConnectionForTable($table)->createQueryBuilder()->select('*')->from($table)->executeQuery();
		$i = 0;
		$chunk = 0;
		$rowCount = 0;
		$rowsToWrite = [];
		while ($row = $rows->fetchAssociative()) {
			$rowsToWrite[] = $row;
			$i++;
			$rowCount++;
			if ($i === $chunkSize) {
				$this->write($dir, $table, $chunk, $rowsToWrite);
				$i = 0;
				$chunk++;
				$rowsToWrite = [];
			}
		}
		if (count($rowsToWrite) > 0) {
			$this->write($dir, $table, $chunk, $rowsToWrite);
		}
		return $rowCount;
	}

	/**
	 * @param string $dir
	 * @param string $table
	 * @return int
	 */
	public function dumpJsonFileToTable(string $dir, string $table): int {
		$connection = $this->connectionPool->getConnectionForTable($table);
		$connection->truncate($table);
		$rowCount = 0;
		foreach ($this->getFiles($dir, $table) as $file) {
			$rows = json_decode(file_get_contents($file->getRealPath()), true);
			$connection->bulkInsert($table, $rows);
			$rowCount += count($rows);
		}
		return $rowCount;
	}

	/**
	 * @param string $dir
	 * @param string $table
	 * @return \Generator|\SplFileInfo[]
	 */
	private function getFiles(string $dir, string $table): Generator {
		/** @var \SplFileInfo $file */
		foreach (new DirectoryIterator($dir) as $file) {
			$pattern = '/^' . preg_quote($table) . '_[0-9]+$/';
			if (preg_match($pattern, $file->getBasename('.json'))) {
				yield $file;
			}
		}
	}

	/**
	 * @param string $dir
	 * @param string $table
	 * @param int $chunk
	 * @param array $rows
	 * @return void
	 */
	private function write(string $dir, string $table, int $chunk, array $rows): void {
		file_put_contents($dir . DIRECTORY_SEPARATOR . $table . '_' . $chunk . '.json', json_encode($rows, JSON_PRETTY_PRINT));
	}
}
