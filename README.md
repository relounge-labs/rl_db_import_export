[![TYPO3 12](https://img.shields.io/badge/TYPO3-12-orange.svg)](https://get.typo3.org/version/12)
![Packagist PHP Version](https://img.shields.io/packagist/dependency-v/re-lounge/rl_db_import_export/php)
![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/relounge-labs/rl_db_import_export/master)
![Packagist License (custom server)](https://img.shields.io/packagist/l/re-lounge/rl_db_import_export)

# Database JSON Import / Export

This TYPO3 extension allows to export and import a database table in JSON format to a file.

## Examples

Export all entries from the table `tt_content` and write the data to `./`. 
Each file contains `100` entries:

```
vendor/bin/typo3cms rl:database:export  tt_content ./ 100
```

Import all data from `./` into the table `tt_content`: 

```
vendor/bin/typo3cms rl:database:import ./ tt_content
```

## PHPUnit

Run all tests in Docker:

```
docker-compose up --build
```
