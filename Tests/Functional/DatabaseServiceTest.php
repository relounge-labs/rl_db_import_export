<?php

declare(strict_types=1);

namespace Relounge\RlDbImportExport\Tests\Functional;

use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\Test;
use Relounge\RlDbImportExport\Service\DatabaseService;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\TestingFramework\Core\Functional\FunctionalTestCase;

class DatabaseServiceTest extends FunctionalTestCase {

	// Define the extensions to test
	protected array $testExtensionsToLoad = [
		'typo3conf/ext/rl_db_import_export',
	];

	private string $resultPath;

	public function setUp(): void {
		// Use sqlite database for testing
		putenv('typo3DatabaseDriver=pdo_sqlite');
		$this->configurationToUseInTestInstance['DB']['Connections']['Default'] = [
			'driver' => 'pdo_sqlite',
			'path' => __DIR__ . DIRECTORY_SEPARATOR . time() . '_phpunit.sqlite',
		];
		$this->resultPath = __DIR__ . '/../../.Build';
		parent::setUp();
	}

	/**
	 * Remove files created during the test.
	 *
	 * @return void
	 */
	public function tearDown(): void {
		$tempDatabaseFile = $this->configurationToUseInTestInstance['DB']['Connections']['Default']['path'];
		if (is_file($tempDatabaseFile)) {
			unlink($tempDatabaseFile);
		}
		parent::tearDown();
	}

	/**
	 * @throws \Doctrine\DBAL\Exception
	 */
	#[Test]
	public function testExport(): void {
		$resourcePath = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Resources';
		$expectedJsonFile = $resourcePath . DIRECTORY_SEPARATOR . 'tt_content_0.json';
		$expectedJson = json_decode(file_get_contents($expectedJsonFile), true);

		// Import test data
		$this->importCSVDataSet($resourcePath . DIRECTORY_SEPARATOR . 'database.csv');
		$connection = GeneralUtility::makeInstance(ConnectionPool::class);
		$service = new DatabaseService($connection);

		// Test export
		$service->dumpTableToJsonFile('tt_content', $this->resultPath, 100);
		$result = json_decode(file_get_contents($this->resultPath . DIRECTORY_SEPARATOR . 'tt_content_0.json'), true);
		Assert::assertEqualsCanonicalizing($expectedJson, $result);

		// Test import
		$service->dumpJsonFileToTable($resourcePath, 'tt_content');
		$rows = $connection->getConnectionForTable('tt_content')->select(['*'], 'tt_content')->fetchAllAssociative();
		Assert::assertCount(2, $rows);
		Assert::assertEqualsCanonicalizing($expectedJson, $rows);
	}
}
